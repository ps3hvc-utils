
CC=gcc
CFLAGS=-Wall -O2 -g -m64
LDFLAGS=-m64

SRC_ps3hvc_hvcall=ps3hvc_dev.c ps3hvc_hvcall.c
OBJ_ps3hvc_hvcall=$(SRC_ps3hvc_hvcall:.c=.o)
TARGET_ps3hvc_hvcall=ps3hvc_hvcall

all: $(TARGET_ps3hvc_hvcall)

$(TARGET_ps3hvc_hvcall): $(OBJ_ps3hvc_hvcall)
	$(CC) $(LDFLAGS) -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -c $<

.PHONY: clean
clean:
	rm -f $(OBJ_ps3hvc_hvcall) $(TARGET_ps3hvc_hvcall)
