
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "ps3hvc_dev.h"

int ps3hvc_dev_open(const char *path)
{
	return open(path, O_RDWR);
}

int ps3hvc_dev_close(int fd)
{
	return close(fd);
}

int ps3hvc_dev_hvcall(int fd, uint64_t number, uint64_t in_args,
	uint64_t out_args, uint64_t args[], int64_t *result)
{
	struct ps3hvc_ioctl_hvcall *arg;
	int size, error;

	size = sizeof(struct ps3hvc_ioctl_hvcall) + (in_args + out_args) * sizeof(uint64_t);

	arg = malloc(size);
	if (!arg)
		return ENOMEM;

	memset(arg, 0, size);
	arg->number = number;
	arg->in_args = in_args;
	arg->out_args = out_args;
	memcpy(arg->args, args, in_args * sizeof(uint64_t));

	error = ioctl(fd, PS3HVC_IOCTL_HVCALL, arg);
	
	if (error == 0) {
		*result = arg->result;

		memcpy(&args[in_args], &arg->args[in_args], out_args * sizeof(uint64_t));
	}

	free(arg);

	return error;
}
