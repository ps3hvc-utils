
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <errno.h>

#include "ps3hvc_dev.h"

#define PS3HVC_HVCALL_VERSION		"0.0.1"

struct opts
{
	char *device_name;
	char *cmd;
	int do_help;
	int do_verbose;
	int do_version;
};

static struct option long_opts[] = {
	{ "help",	no_argument, NULL, 'h' },
	{ "verbose",	no_argument, NULL, 'v' },
	{ "version",	no_argument, NULL, 'V' },
	{ NULL, 0, NULL, 0 }
};

/*
 * usage
 */
static void usage(void) {
	fprintf(stderr,
		"Usage: ps3hvc_hvcall [OPTIONS] DEVICE COMMAND [ARGS]\n"
		"\n"
		"Options:\n"
		"	-h, --help					Show this message and exit\n"
		"	-v, --verbose					Increase verbosity\n"
		"	-V, --version					Show version information and exit\n"
		"Commands:\n"
		"	undocumented_function_8				Returns current uptime\n"
		"	construct_event_receive_port			Constructs outlet\n"
		"	destruct_event_receive_port OUTLETID		Destructs outlet\n"
		"	get_repo_node_val LPARID KEY0 KEY1 KEY2 KEY3	Returns repository node value\n"
		"	panic ARG1					Panics\n"
		"\n\n"
		"Simple example: Reboot after panic:\n"
		"	ps3hvc_hvcall /dev/ps3hvc panic 1\n");
}

/*
 * version
 */
static void version(void)
{
	fprintf(stderr,
		"ps3hvc_hvcall " PS3HVC_HVCALL_VERSION "\n"
		"Copyright (C) 2011 graf_chokolo <grafchokolo@googlemail.com>\n"
		"This is free software.  You may redistribute copies of it "
		"under the terms of\n"
		"the GNU General Public License 2 "
		"<http://www.gnu.org/licenses/gpl2.html>.\n"
		"There is NO WARRANTY, to the extent permitted by law.\n");
}

/*
 * process_opts
 */
static int process_opts(int argc, char **argv, struct opts *opts)
{
	int c;

	while ((c = getopt_long(argc, argv, "hvV", long_opts, NULL)) != -1) {
		switch (c) {
		case 'h':
		case '?':
			opts->do_help = 1;
			return 0;

		case 'v':
			opts->do_verbose++;
			break;

		case 'V':
			opts->do_version = 1;
			return 0;

		default:
			fprintf(stderr, "Invalid command option: %c\n", c);
			return -1;
		}
	}

	if (optind >= argc) {
		fprintf(stderr, "No device specified\n");
		return -1;
	}

	opts->device_name = argv[optind];
	optind++;

	if (optind >= argc) {
		fprintf(stderr, "No command specified\n");
		return -1;
	}

	opts->cmd = argv[optind];
	optind++;

	return 0;
}

/*
 * cmd_undocumented_function_8
 */
static int cmd_undocumented_function_8(int fd, struct opts *opts, int argc, char **argv)
{
	uint64_t args[1];
	int64_t result;
	int error;

	error = ps3hvc_dev_hvcall(fd, PS3HVC_HVCALL_UNDOCUMENTED_FUNCTION_8, 0, 1, args, &result);

	if (error)
		fprintf(stderr, "%s: %s\n", opts->device_name, strerror(errno));
	else if (result)
		fprintf(stderr, "hvcall result: 0x%016lx\n", result);
	else
		fprintf(stdout, "0x%016lx\n", args[0]);

	return error;
}

/*
 * cmd_construct_event_receive_port
 */
static int cmd_construct_event_receive_port(int fd, struct opts *opts, int argc, char **argv)
{
	uint64_t args[1];
	int64_t result;
	int error;

	error = ps3hvc_dev_hvcall(fd, PS3HVC_HVCALL_CONSTRUCT_EVENT_RECEIVE_PORT, 0, 1, args, &result);

	if (error)
		fprintf(stderr, "%s: %s\n", opts->device_name, strerror(errno));
	else if (result)
		fprintf(stderr, "hvcall result: 0x%016lx\n", result);
	else
		fprintf(stdout, "0x%016lx\n", args[0]);

	return error;
}

/*
 * cmd_destruct_event_receive_port
 */
static int cmd_destruct_event_receive_port(int fd, struct opts *opts, int argc, char **argv)
{
	uint64_t outletid;
	char *endptr;
	uint64_t args[1];
	int64_t result;
	int error;

	if (optind >= argc) {
		fprintf(stderr, "No outlet identifier specified\n");
		return -1;
	}

	outletid = strtoull(argv[optind], &endptr, 0);
	if (*endptr != '\0') {
		fprintf(stderr, "Invalid outlet identifier specified: %s\n", argv[optind]);
		return -1;
	}

	optind++;

	args[0] = outletid;

	error = ps3hvc_dev_hvcall(fd, PS3HVC_HVCALL_DESTRUCT_EVENT_RECEIVE_PORT, 1, 0, args, &result);

	if (error)
		fprintf(stderr, "%s: %s\n", opts->device_name, strerror(errno));
	else
		fprintf(stderr, "hvcall result: 0x%016lx\n", result);

	return error;
}

/*
 * cmd_get_repo_node_val
 */
static int cmd_get_repo_node_val(int fd, struct opts *opts, int argc, char **argv)
{
	uint64_t lpar_id, key[4];
	char *endptr, *opt;
	uint64_t args[7];
	int64_t result;
	int i, error;

	if (optind >= argc) {
		fprintf(stderr, "No LPAR identifier specified\n");
		return -1;
	}

	lpar_id = strtoull(argv[optind], &endptr, 0);
	if (*endptr != '\0') {
		fprintf(stderr, "Invalid LPAR identifier specified: %s\n", argv[optind]);
		return -1;
	}

	optind++;

	for (i = 0; i < 4; i++) {
		if (optind >= argc) {
			fprintf(stderr, "No key #%d specified\n", i);
			return -1;
		}

		opt = argv[optind++];
		key[i] = strtoull(opt, &endptr, 0);
		if ((*opt == '\0') || (*endptr != '\0')) {
			fprintf(stderr, "Invalid key #%d specified: %s\n", i, opt);
			return -1;
		}
	}

	args[0] = lpar_id;
	memcpy(&args[1], key, sizeof(key));

	error = ps3hvc_dev_hvcall(fd, PS3HVC_HVCALL_GET_REPO_NODE_VAL, 5, 2, args, &result);

	if (error) {
		fprintf(stderr, "%s: %s\n", opts->device_name, strerror(errno));
	} else if (result) {
		fprintf(stderr, "hvcall result: 0x%016lx\n", result);
	} else {
		fprintf(stdout, "0x%016lx 0x%016lx\n", args[5], args[6]);
	}

	return error;
}

/*
 * cmd_panic
 */
static int cmd_panic(int fd, struct opts *opts, int argc, char **argv)
{
	uint64_t arg1;
	char *endptr;
	uint64_t args[1];
	int64_t result;
	int error;

	if (optind >= argc) {
		fprintf(stderr, "No ARG1 specified\n");
		return -1;
	}

	arg1 = strtoull(argv[optind], &endptr, 0);
	if (*endptr != '\0') {
		fprintf(stderr, "Invalid ARG1 specified: %s\n", argv[optind]);
		return -1;
	}

	optind++;

	args[0] = arg1;

	error = ps3hvc_dev_hvcall(fd, PS3HVC_HVCALL_PANIC, 1, 0, args, &result);

	if (error)
		fprintf(stderr, "%s: %s\n", opts->device_name, strerror(errno));
	else
		fprintf(stderr, "hvcall result: 0x%016lx\n", result);

	return error;
}

/*
 * main
 */
int main(int argc, char **argv)
{
	struct opts opts;
	int fd = 0, error = 0;

	memset(&opts, 0, sizeof(opts));

	if (process_opts(argc, argv, &opts)) {
		usage();
		error = 1;
		goto done;
	}

	if (opts.do_help) {
		usage();
		goto done;
	} else if (opts.do_version) {
		version();
		goto done;
	}

	fd = ps3hvc_dev_open(opts.device_name);
	if (fd < 0) {
		fprintf(stderr, "%s: %s\n", opts.device_name, strerror(errno));
		error = 2;
		goto done;
	}

	if (!strcmp(opts.cmd, "undocumented_function_8")) {
		error = cmd_undocumented_function_8(fd, &opts, argc, argv);
	} else if (!strcmp(opts.cmd, "construct_event_receive_port")) {
		error = cmd_construct_event_receive_port(fd, &opts, argc, argv);
	} else if (!strcmp(opts.cmd, "destruct_event_receive_port")) {
		error = cmd_destruct_event_receive_port(fd, &opts, argc, argv);
	} else if (!strcmp(opts.cmd, "get_repo_node_val")) {
		error = cmd_get_repo_node_val(fd, &opts, argc, argv);
	} else if (!strcmp(opts.cmd, "panic")) {
		error = cmd_panic(fd, &opts, argc, argv);
	} else {
		usage();
		error = 1;
		goto done;
	}

	if (error)
		error = 3;

done:

	if (fd >= 0)
		ps3hvc_dev_close(fd);

	exit(error);
}
